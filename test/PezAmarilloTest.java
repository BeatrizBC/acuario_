import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import greenfoot.junitUtils.WorldCreator;

class PezAmarilloTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testAct() {
		// Given
		Acuario acuario
        = (Acuario)WorldCreator.getWorld(Acuario.class);
        PezAmarillo pez = new PezAmarillo();
        acuario.addObject(pez, 100, 150);
        
        // When
        pez.act();
        
        // Then
        assertEquals(101, pez.getX());
        assertEquals(150, pez.getY());
     
	}

}
