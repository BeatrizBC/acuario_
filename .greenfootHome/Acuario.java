import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Acuario extends World {

    public Acuario() {           
        super(600, 400, 1); 
        prepare();
    }

    private void prepare() {        
        PezAmarillo pezAmarillo = new PezAmarillo();
        addObject(pezAmarillo,171,108);
        
        PezAmarillo pezAmarillo2 = new PezAmarillo();
        addObject(pezAmarillo2,71,308);
        
        Langosta langosta = new Langosta();
        addObject(langosta,293,250);
    }
}
